package com.paul.projects.cinema.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.paul.projects.cinema.domain.Movie;

@RestController
@RequestMapping("movies")
public class MovieController {
	
	@GetMapping(produces="application/json")
	public List<Movie> getMovies() {
		List<Movie> listMovie = new ArrayList<Movie>();
		listMovie.add(new Movie("1", "Paul Zacarias", "1990"));
		return listMovie;
	}

}
