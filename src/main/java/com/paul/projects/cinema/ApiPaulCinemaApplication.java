package com.paul.projects.cinema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPaulCinemaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPaulCinemaApplication.class, args);
	}

}
